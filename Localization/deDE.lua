--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/EZCraft/localization/
--

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local T = LibStub( "WAR-AceLocale-3.0" ):NewLocale( "EZCraftX", "deDE", false, debug )

if not T then return end
  
T["Apply"] = L"�bernehmen"
T["Armor"] = L"R�stung"
T["ascending"] = L"aufsteigend"
T["Awesome"] = L"Besonders"
T["Awesome Result"] = L"Besonderes Ergebnis"
T["Ballistics"] = L"Ballistik"
T["Cancel"] = L"Abbrechen"
T["Corporeal Resist"] = L"K�rperresistenz"
T["Defaults"] = L"Standard"
T["descending"] = L"absteigend"
T["Elemental Resist"] = L"Elementarresistenz"
T["Good"] = L"Gut"
T["Good Result"] = L"Gutes Ergebnis"
T["Initiative"] = L"Initiative"
T["Intelligence"] = L"Intelligenz"
T["Level"] = L"Level"
T["loaded"] = L"geladen"
T["Maximum items per level"] = L"Maximale Anzahl an Gegenst�nden je Level"
T["Minimum Rank:"] = L"Mindestrang:"
T["more... (%s)"] = L"mehr... (%s)"
T["Regular"] = L"Normal"
T["Regular Result"] = L"Normales Ergebnis"
T["%s chance for critical success"] = L"%s Chance auf kritischen Erfolg"
T["Show power preview"] = L"Show power preview" -- Requires localization
T["Show result preview"] = L"Ergebnisvorschau anzeigen"
T["Sort order items"] = L"Sortierung Gegenst�nde"
T["Sort order level"] = L"Sortierung Level"
T["Spirit Resist"] = L"Geistresistenz"
T["%s power added to recipe from all ingredients"] = L"%s power added to recipe from all ingredients" -- Requires localization
T["%s power added to recipe from this ingredient"] = L"%s power added to recipe from this ingredient" -- Requires localization
T["Strength"] = L"St�rke"
T["Talisman"] = L"Talisman"
T["Toughness"] = L"Widerstand"
T["type /ecx for options"] = L"/ecx f�r Optionen"
T["Weapon Skill"] = L"Kampfgeschick"
T["Willpower"] = L"Willenskraft"
T["Wounds"] = L"Leben"

