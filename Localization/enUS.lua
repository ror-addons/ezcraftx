--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/EZCraft/localization/
--

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local T = LibStub( "WAR-AceLocale-3.0" ):NewLocale( "EZCraftX", "enUS", true, debug )

if not T then return end

T["Apply"] = L"Apply"
T["Armor"] = L"Armor"
T["ascending"] = L"ascending"
T["Awesome"] = L"Awesome"
T["Awesome Result"] = L"Awesome Result"
T["Ballistics"] = L"Ballistics"
T["Cancel"] = L"Cancel"
T["Corporeal Resist"] = L"Corporeal Resist"
T["Defaults"] = L"Defaults"
T["descending"] = L"descending"
T["Elemental Resist"] = L"Elemental Resist"
T["Good"] = L"Good"
T["Good Result"] = L"Good Result"
T["Initiative"] = L"Initiative"
T["Intelligence"] = L"Intelligence"
T["Level"] = L"Level"
T["loaded"] = L"loaded"
T["Maximum items per level"] = L"Maximum items per level"
T["Minimum Rank:"] = L"Minimum Rank:"
T["more... (%s)"] = L"more... (%s)"
T["Regular"] = L"Regular"
T["Regular Result"] = L"Regular Result"
T["%s chance for critical success"] = L"%s chance for critical success"
T["Show power preview"] = L"Show power preview"
T["Show result preview"] = L"Show result preview"
T["Sort order items"] = L"Sort order items"
T["Sort order level"] = L"Sort order level"
T["Spirit Resist"] = L"Spirit Resist"
T["%s power added to recipe from all ingredients"] = L"%s power added to recipe from all ingredients"
T["%s power added to recipe from this ingredient"] = L"%s power added to recipe from this ingredient"
T["Strength"] = L"Strength"
T["Talisman"] = L"Talisman"
T["Toughness"] = L"Toughness"
T["type /ecx for options"] = L"type /ecx for options"
T["Weapon Skill"] = L"Weapon Skill"
T["Willpower"] = L"Willpower"
T["Wounds"] = L"Wounds"

