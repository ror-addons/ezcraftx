--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/EZCraft/localization/
--

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local T = LibStub( "WAR-AceLocale-3.0" ):NewLocale( "EZCraftX", "itIT", false, debug )

if not T then return end

T["Apply"] = L"Applica"
T["Armor"] = L"Armatura"
T["ascending"] = L"ascendente"
T["Awesome"] = L"Fantastico"
T["Awesome Result"] = L"Risultato Fantastico"
T["Ballistics"] = L"Balistica"
T["Cancel"] = L"Cancella"
T["Corporeal Resist"] = L"Resistenza Corporea"
T["Defaults"] = L"Predefiniti"
T["descending"] = L"discendente"
T["Elemental Resist"] = L"Resistenza Elementale"
T["Good"] = L"Buono"
T["Good Result"] = L"Risultato Buono"
T["Initiative"] = L"Iniziativa"
T["Intelligence"] = L"Acume"
T["Level"] = L"Livello"
T["loaded"] = L"caricato"
T["Maximum items per level"] = L"Massimo numero di oggetti per livello"
T["Minimum Rank:"] = L"Livello Minimo:"
T["more... (%s)"] = L"altro... (%s)"
T["Regular"] = L"Normale"
T["Regular Result"] = L"Risultato Normale"
T["%s chance for critical success"] = L"%s possibiltÓ di successo critico"
T["Show power preview"] = L"Show power preview" -- Requires localization
T["Show result preview"] = L"Visualizza l'anteprima del risultato"
T["Sort order items"] = L"Ordinamento degli oggetti"
T["Sort order level"] = L"Ordinamento del livello"
T["Spirit Resist"] = L"Resistenza Spirituale"
T["%s power added to recipe from all ingredients"] = L"%s power added to recipe from all ingredients" -- Requires localization
T["%s power added to recipe from this ingredient"] = L"%s power added to recipe from this ingredient" -- Requires localization
T["Strength"] = L"Forza"
T["Talisman"] = L"Talismano"
T["Toughness"] = L"Resistenza"
T["type /ecx for options"] = L"scrivi /ecx per le opzioni"
T["Weapon Skill"] = L"Armi"
T["Willpower"] = L"VolontÓ"
T["Wounds"] = L"Ferite"

